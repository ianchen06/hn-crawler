import asyncio
import json

import aiohttp
import aiofiles

CONCURRENCY = 10
DATA_DIR = 'data'
NO_ITEMS = 10000

q = asyncio.Queue()

async def worker(name, queue):
    print(f"Started worker {name}")
    async with aiohttp.ClientSession() as session:
        while True:
            # Get a "work item" out of the queue.
            item_id = await queue.get()

            # Sleep for the "sleep_for" seconds.
            data = await fetch_json(session, f"https://hacker-news.firebaseio.com/v0/item/{item_id}.json")
            async with aiofiles.open(f'./{DATA_DIR}/{item_id}.json', mode='w') as f:
                contents = await f.write(json.dumps(data, ensure_ascii=False))

            # Notify the queue that the "work item" has been processed.
            queue.task_done()

            print(f"[{name}][{item_id}]")
            # print(data)

async def fetch_text(session, url):
    async with session.get(url) as response:
        return await response.text()

async def fetch_json(session, url):
    async with session.get(url) as response:
        return await response.json()

async def main():
    async with aiohttp.ClientSession() as session:
        total = await fetch_text(session, 'https://hacker-news.firebaseio.com/v0/maxitem.json')
        total = int(total)
        for x in range(total, total - NO_ITEMS, -1):
            q.put_nowait(x)
        # Create three worker tasks to process the queue concurrently.
    tasks = []
    for i in range(CONCURRENCY):
        task = asyncio.create_task(worker(f'worker-{i}', q))
        tasks.append(task)
    await q.join()
    # Cancel our worker tasks.
    for task in tasks:
        task.cancel()
    # Wait until all worker tasks are cancelled.
    await asyncio.gather(*tasks, return_exceptions=True)

loop = asyncio.get_event_loop()
loop.run_until_complete(main())
